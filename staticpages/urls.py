from django.urls import path
from . import views

app_name = 'delicia'
urlpatterns = [
    path('', views.index, name='index'),
    path('receitas/', views.list_staticpages, name='receitas'),
    path('about/', views.about, name='about'),    
    path('receitas/<int:receita_id>/', views.detail_receita, name='detail_receita'),
    path('receitas/create_receita/', views.create_receita, name='create_receita'),
    path('receitas/update/<int:receita_id>/', views.update_receita, name='update_receita'),    
    path('delete/<int:receita_id>/', views.delete_receita, name='delete_receita'),
    path('review/<int:receita_id>/', views.create_review, name='review'),]
