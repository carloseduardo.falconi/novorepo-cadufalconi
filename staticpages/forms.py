from django.forms import ModelForm
from .models import Review



class ReviewForm(ModelForm):
    class Meta:
        model = Review
        fields = [
            'author',
            'text',
            
        ]
        labels = {
            'author': 'Seu nome',
            'text': 'Mensagem',
        }