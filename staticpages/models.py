from django.db import models
from django.conf import settings


class receita(models.Model):
    name = models.CharField(max_length=255)
    poster_url = models.URLField(max_length=200, null=True)
    ingredientes = models.CharField(max_length=600, null=True)
    mdp = models.CharField(max_length=600, null=True)
    

    def __str__(self):
        return f'{self.name}'


class Review(models.Model):
    author = models.ForeignKey(settings.AUTH_USER_MODEL,
                               on_delete=models.CASCADE)
    text = models.CharField(max_length=255)
    receita = models.ForeignKey(receita, on_delete=models.CASCADE)

    def __str__(self):
        return f'"{self.text}" - {self.author.username}'
