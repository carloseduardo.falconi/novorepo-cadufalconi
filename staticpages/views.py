from __future__ import unicode_literals
from django.http import HttpResponse, HttpResponseRedirect, request
from django.shortcuts import render
from .models import receita, Review
from django.shortcuts import render, get_object_or_404
from django.urls import reverse, reverse_lazy
from .forms import ReviewForm


def index(request):
    context = {}
    return render(request, 'staticpages/index.html', context)

def receitas(request):
    context = {}
    return render(request, 'staticpages/receitas.html', context)

def about(request):
    context = {}
    return render(request, 'staticpages/about.html', context)

def list_staticpages(request):
    receita_list = receita.objects.all()
    context = {'receita_list': receita_list}
    return render(request, 'staticpages/receitas.html', context)

def detail_receita(request, receita_id):
    receitas = get_object_or_404(receita, pk=receita_id)
    context = {'receitas': receitas}
    return render(request, 'staticpages/detail.html', context)

def search_receita(request):
    context = {}
    if request.GET.get('query', False):
        search_term = request.GET['query'].lower()
        receita_list = receita.objects.filter(name__icontains=search_term)
        context = {"receita_list": receita_list}
    return render(request, 'receita/search.html', context)

def create_receita(request):
    if request.method == 'POST':
        receita_name = request.POST['name']
        receita_poster_url = request.POST['poster_url']
        receita_ingredientes = request.POST['ingredientes']
        receita_mdp = request.POST['mdp']
        Receitas = receita (name=receita_name,
                      poster_url=receita_poster_url,
                      ingredientes=receita_ingredientes,
                      mdp=receita_mdp)
        Receitas.save()
        return HttpResponseRedirect(
            reverse('delicia:detail_receita', args=(Receitas.id, )))
    else:
        return render(request, 'staticpages/create_receita.html', {})


def update_receita(request, receita_id):
    receitas = get_object_or_404(receita, pk=receita_id)
    if request.method == "POST":
        receitas.name = request.POST['name']
        receitas.poster_url = request.POST['poster_url']
        receitas.ingredientes = request.POST['ingredientes']
        receitas.mdp = request.POST['mdp']
        receitas.save()
        return HttpResponseRedirect(
            reverse('delicia:detail', args=(receita.id, )))
    context = {'receitas': receitas}
    return render(request, 'staticpages/update.html', context)


def delete_receita(request, receita_id):
    receitas = get_object_or_404(receita, pk=receita_id)
    if request.method == "POST":
        receitas.delete()
        return HttpResponseRedirect(reverse('delicia:receitas'))
    context = {'receitas': receitas}
    return render(request, 'staticpages/delete.html', context)


def create_review(request, receita_id):
    receitas = get_object_or_404(receita, pk=receita_id)
    if request.method == 'POST':
        form = ReviewForm(request.POST)
        if form.is_valid():
            review_author = form.cleaned_data['author']
            review_text = form.cleaned_data['text']
            review = Review(author=review_author,
                            text=review_text,
                            receita=receitas)
            review.save()
            return HttpResponseRedirect(
                reverse('delicia:detail_receita', args=(receita_id, )))
    else:
        form = ReviewForm()
    context = {'form': form, 'receitas': receitas}
    return render(request, 'staticpages/review.html', context)

