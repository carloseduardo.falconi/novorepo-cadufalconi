from django.contrib import admin

from .models import receita, Review

admin.site.register(receita)
admin.site.register(Review)